#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PoseStamped.h>
#include <gazebo_msgs/SetModelState.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <std_srvs/Empty.h>
#include <com/Mavlink.h>
#include <mavlink/v1.0/common/mavlink.h>
#include "com/utils.h"
#include <nav_msgs/Odometry.h>
#include <common_msgs/state.h>

//#define DIRECTSET
ros::Publisher pose_pub,cmd_pub;

void get_state(const nav_msgs::Odometry::ConstPtr &msg)
{
    geometry_msgs::PoseStamped reb;
    reb.pose = msg->pose.pose;
    pose_pub.publish(reb);
}

void get_cmd(const common_msgs::state::ConstPtr& msg)
{
#ifndef DIRECTSET
    geometry_msgs::PoseStamped reb;
    reb.pose.position.x = msg->pos.x;
    reb.pose.position.y = msg->pos.y;
    reb.pose.position.z = msg->pos.z;
    cmd_pub.publish(reb);
#else
    gazebo_msgs::ModelState reb;
    reb.model_name = std::string("quadrotor");
    reb.pose.position.x = msg->pos.x;
    reb.pose.position.y = msg->pos.y;
    reb.pose.position.z = msg->pos.z;
    cmd_pub.publish(reb);
#endif
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "msg_rangs");
    ros::NodeHandle nh;
    ros::Subscriber state_sub = nh.subscribe("/ground_truth/state", 1, get_state);
    pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/mavros/position/local",1);

    ros::Subscriber cmd_sub = nh.subscribe("rt_ref_gen/current_state", 1, get_cmd);
#ifndef DIRECTSET
    cmd_pub = nh.advertise<geometry_msgs::PoseStamped>("/command/pose",1);
#else
    cmd_pub = nh.advertise<gazebo_msgs::ModelState>("/gazebo/set_model_state", 1);
#endif
    ros::spin();

    return 0;
}
