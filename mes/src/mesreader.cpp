#include "mes/mesreader.h"

MesReader::MesReader(ros::NodeHandle & nh)
{
    x = 0;
    y = 0;
    z = 0;
    a = 0;
    b = 0;
    c = 0;
    d = 0;
#ifdef USEVICON
    m_sub = nh.subscribe("/vicon/test_vicon/test_vicon", 1, &MesReader::msgCallback, this);
#else
    m_sub = nh.subscribe("/vrpn_client_node/RigidBody01/pose", 1, &MesReader::msgCallback, this);
#endif
    m_pub = nh.advertise<com::Mavlink>("/mavlinkMsg",10);
    m_timer = nh.createTimer(ros::Duration(0.033), &MesReader::timerCallback, this);
}

MesReader::~MesReader()
{}

void MesReader::timerCallback(const ros::TimerEvent&)
{
    mavlink_message_t msg;
    mavlink_msg_vicon_position_estimate_pack(254, 50, &msg, 0, x, y, z, a, b, c, d);
    com::MavlinkPtr rmsg = boost::make_shared<com::Mavlink>();
    mavutils::copy_mavlink_to_ros(&msg, rmsg);
    m_pub.publish(rmsg);

}

#ifdef USEVICON
void MesReader::msgCallback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
    x = msg->transform.translation.x*1000;
    y = msg->transform.translation.y*1000;
    z = msg->transform.translation.z*1000;
    a = msg->transform.rotation.x*1000;
    b = msg->transform.rotation.y*1000;
    c = msg->transform.rotation.z*1000;
    d = msg->transform.rotation.w*1000;
}
#else
void MesReader::msgCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    x = msg->pose.position.x*1000;
    y = msg->pose.position.y*1000;
    z = msg->pose.position.z*1000;
    a = msg->pose.orientation.x*1000;
    b = msg->pose.orientation.y*1000;
    c = msg->pose.orientation.z*1000;
    d = msg->pose.orientation.w*1000;
}
#endif
