#include <mes/mesreader.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mes_node");
    ros::NodeHandle nh;
    MesReader mesR(nh);
    ros::spin();
    return 0;
}
