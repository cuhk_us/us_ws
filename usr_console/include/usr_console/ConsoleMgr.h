#ifndef CONSOLEMGR_H
#define CONSOLEMGR_H
#include <ros/ros.h>
#include <boost/asio.hpp>
#include <usr_console/AsyncIn.h>
#include <pluginlib/class_loader.h>
#include <usr_console/ConsoleMsgPlugin.h>
class ConsoleMgr
{
public:
    ConsoleMgr(ros::NodeHandle &nh);
    ~ConsoleMgr();
    void exec();
    void handleStdin(const std::string &input);

private:
    ros::NodeHandle m_nh;
    boost::asio::io_service m_io;
    std::unique_ptr<boost::asio::io_service::work> m_work;
    boost::asio::deadline_timer m_t;
    int m_timer_duration;
    AsyncIn* m_input;
    std::string m_cmd;
    std::vector<double> m_parameters;
    pluginlib::ClassLoader<consoleplugin::ConsoleMsgPlugin> m_plugin_loader;
    std::vector<consoleplugin::ConsoleMsgPlugin::Ptr> m_loaded_plugins;
    std::map<std::string, consoleplugin::ConsoleMsgPlugin::message_handler> m_route_table;

private:
    void rosSpinOnce(const boost::system::error_code &error);
    void stdinCallback(const std::string msg);
    void endSpin();
    bool readCommand(const std::string &input_str, std::string &cmd, std::vector<double> &parameters);
    std::string trim(const std::string& str, const std::string& whitespace = " \t");
    void add_plugin(std::string &pl_name);
};

#endif // CONSOLEMGR_H
