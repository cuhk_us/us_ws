#include <usr_console/ConsoleMgr.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "console_node");
    ros::NodeHandle nh;
    ConsoleMgr c_mgr(nh);
    c_mgr.exec();
    return 0;
}
