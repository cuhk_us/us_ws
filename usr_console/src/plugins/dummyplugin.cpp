#include <usr_console/ConsoleMsgPlugin.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Point.h>
#include <gazebo_msgs/GetModelState.h>
#include <std_msgs/Bool.h>
namespace consoleplugin {

class DummyPlugin : public ConsoleMsgPlugin
{
public:
    DummyPlugin()
    {
        ROS_INFO_NAMED("dummy", "dummy constructor");
    }

    ~DummyPlugin()
    {

    }

    void initialize(ros::NodeHandle &nh)
    {
        ROS_INFO_NAMED("dummy", "initialize");
        m_is_circle = false;
        m_nh = ros::NodeHandle(nh, "dummy");
        m_obs_client = nh.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
        m_pos_cmd_pub = m_nh.advertise<geometry_msgs::Point>("pos_cmd",1);
        m_engage_pub = m_nh.advertise<std_msgs::Bool>("/engage_cmd",1);
        m_randfly_timer = nh.createTimer(ros::Duration(7),&DummyPlugin::randfly,this,false,false);
        m_target_timer = nh.createTimer(ros::Duration(0.1),&DummyPlugin::genTarget,this,false,false);
    }

    std::string getName()
    {
        return "dummy";
    }

    const message_map get_msg_handlers() {
        return {
            MESSAGE_HANDLER("engage", &DummyPlugin::handle_engage),
            MESSAGE_HANDLER("shutdown", &DummyPlugin::handle_shutdown),
            MESSAGE_HANDLER("position", &DummyPlugin::handle_position),
            MESSAGE_HANDLER("randfly", &DummyPlugin::handle_randfly),
            MESSAGE_HANDLER("follow", &DummyPlugin::handle_follow),
            MESSAGE_HANDLER("circle", &DummyPlugin::handle_circle)
        };
    }

private:
    void randfly(const ros::TimerEvent&)
    {
        geometry_msgs::Point msg;
        msg.x = fRand(-8, 8);
        msg.y = fRand(-8, 8);
        msg.z = fRand(0.5, 1.2);
        m_pos_cmd_pub.publish(msg);
    }

    void genTarget(const ros::TimerEvent&)
    {
        static double theta = 0;
        m_obs_srv.request.model_name = "unit_cylinder_15";
        if (m_obs_client.call(m_obs_srv))
        {
            geometry_msgs::Point msg;
            if (!m_is_circle)
            {
                msg.x = m_obs_srv.response.pose.position.x;
                msg.y = m_obs_srv.response.pose.position.y;
            }
            else
            {
                msg.x = m_obs_srv.response.pose.position.x+5*cos(theta);
                msg.y = m_obs_srv.response.pose.position.y+5*sin(theta);
                theta += 0.06;
            }
            msg.z = 0.8;
            m_pos_cmd_pub.publish(msg);
        }

    }

    void handle_randfly(const std::vector<double> &parameters)
    {
        m_randfly_timer.start();
    }

    void handle_follow(const std::vector<double> &parameters)
    {
        m_is_circle = false;
        m_target_timer.start();
    }

    void handle_circle(const std::vector<double> &parameters)
    {
        m_is_circle = true;
        m_target_timer.start();
    }

    void handle_engage(const std::vector<double> &parameters)
    {
        std_srvs::Empty::Request eReq;
        std_srvs::Empty::Response eRes;
        ros::service::call("engage", eReq, eRes);

        std_msgs::Bool e_msg;
        e_msg.data = true;
        m_engage_pub.publish(e_msg);
    }

    void handle_shutdown(const std::vector<double> &parameters)
    {
        std_srvs::Empty::Request eReq;
        std_srvs::Empty::Response eRes;
        ros::service::call("shutdown", eReq, eRes);
    }

    void handle_position(const std::vector<double> &parameters)
    {
        if (parameters.size()!=3)
        {
            std::cout<<"Your input has: "<<parameters.size()<<" numbers. Please input 3 numbers."<<std::endl;
            return;
        }
        geometry_msgs::Point msg;
        msg.x = parameters[0];
        msg.y = parameters[1];
        msg.z = parameters[2];
        m_pos_cmd_pub.publish(msg);
    }

    double fRand(double fMin, double fMax)
    {
        double f = (double)rand() / RAND_MAX;
        return fMin + f * (fMax - fMin);
    }

private:
    ros::NodeHandle m_nh;
    ros::Publisher  m_pos_cmd_pub, m_engage_pub;
    ros::Timer m_randfly_timer;
    ros::Timer m_target_timer;
    gazebo_msgs::GetModelState m_obs_srv;
    ros::ServiceClient m_obs_client;
    bool m_is_circle;
};

} // namespace consoleplugin

PLUGINLIB_EXPORT_CLASS(consoleplugin::DummyPlugin, consoleplugin::ConsoleMsgPlugin)

