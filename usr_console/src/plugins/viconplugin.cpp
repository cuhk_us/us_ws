#include <usr_console/ConsoleMsgPlugin.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Point.h>
#include <gazebo_msgs/GetModelState.h>
#include <geometry_msgs/TransformStamped.h>
namespace consoleplugin {
//#define USECAR
class ViconPlugin : public ConsoleMsgPlugin
{
public:
    ViconPlugin()
    {
        ROS_INFO_NAMED("vicon", "vicon constructor");
    }

    ~ViconPlugin()
    {

    }

    void initialize(ros::NodeHandle &nh)
    {
        ROS_INFO_NAMED("vicon", "initialize");
        m_wand_pos_initialized = false;
        m_nh = ros::NodeHandle(nh, "vicon");
        m_pos_cmd_pub = m_nh.advertise<geometry_msgs::Point>("/dummy/pos_cmd",1);
        m_target_timer = m_nh.createTimer(ros::Duration(0.25),&ViconPlugin::genTarget,this,false,false);
    }

    std::string getName()
    {
        return "vicon";
    }

    const message_map get_msg_handlers() {
        return {
            MESSAGE_HANDLER("follow_wand", &ViconPlugin::follow_wand)
        };
    }

private:
    void follow_wand(const std::vector<double> &parameters)
    {
#ifdef USECAR
        m_sub = m_nh.subscribe("/vicon/car/car", 1, &ViconPlugin::get_wand, this);
#else
        m_sub = m_nh.subscribe("/vicon/cmd_wind/cmd_wind", 1, &ViconPlugin::get_wand, this);
#endif
    }

    void get_wand(const geometry_msgs::TransformStamped::ConstPtr& msg)
    {
        m_pc[0] = msg->transform.translation.x;
        m_pc[1] = msg->transform.translation.y;
        m_pc[2] = msg->transform.translation.z;
        if (!m_wand_pos_initialized)
        {
            m_wand_pos_initialized = true;
            m_p0[0] = m_pc[0];
            m_p0[1] = m_pc[1];
            m_p0[2] = m_pc[2];
            m_target_timer.start();

        }
    }

    void genTarget(const ros::TimerEvent&)
    {
        geometry_msgs::Point msg;
#ifdef USECAR
        msg.x = m_pc[0];
        msg.y = m_pc[1];
        msg.z = 0.8;
#else
        msg.x = (m_pc[0]-m_p0[0])/0.5*2.4;
        msg.y = (m_pc[1]-m_p0[1])/0.5*2.4;
        msg.z = (m_pc[2]-m_p0[2])*2;
#endif

        m_pos_cmd_pub.publish(msg);
        std::cout<<msg.x<<" "<<msg.y<<" "<<msg.z<<std::endl;
    }

    double fRand(double fMin, double fMax)
    {
        double f = (double)rand() / RAND_MAX;
        return fMin + f * (fMax - fMin);
    }

private:
    ros::NodeHandle m_nh;
    ros::Publisher  m_pos_cmd_pub;
    ros::Timer m_target_timer;
    ros::Subscriber m_sub;
    bool m_wand_pos_initialized;
    double m_p0[3];
    double m_pc[3];
};

} // namespace consoleplugin

PLUGINLIB_EXPORT_CLASS(consoleplugin::ViconPlugin, consoleplugin::ConsoleMsgPlugin)

