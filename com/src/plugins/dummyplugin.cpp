#include <com/MavrosMsgPlugin.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Point.h>
#include <gazebo_msgs/GetModelState.h>
#include "com/serialsocket.h"

namespace mavros {

class DummyPlugin : public MavrosPlugin
{
public:
    DummyPlugin()
    {
        ROS_INFO_NAMED("dummy", "dummy constructor");
    }

    ~DummyPlugin()
    {
        ROS_INFO_NAMED("dummy", "dummy destructor");
    }

    void initialize(ComSocket *socket, ros::NodeHandle &nh)
    {
        ROS_INFO_NAMED("dummy", "initialize");
        m_forward_socket = socket;
    }

    std::string getName()
    {
        return "dummy";
    }

    const message_map get_msg_handlers() {
        return {
            MESSAGE_HANDLER(MAVLINK_MSG_ID_HEARTBEAT, &DummyPlugin::test)
        };
    }

private:
    void test(const mavlink_message_t *msg, uint8_t sysid, uint8_t compid)
    {
        ROS_INFO_NAMED("dummy", "Heartbeat");
        mavlink_message_t mmsg;
        mavlink_msg_command_long_pack(254, 50, &mmsg, 1, 0, MAV_CMD_NAV_TAKEOFF, 1, 0, 0, 0, 0, 0, 0, 0);
        m_forward_socket->send_msg(mmsg);
    }
};

} // namespace mavros

PLUGINLIB_EXPORT_CLASS(mavros::DummyPlugin, mavros::MavrosPlugin)


