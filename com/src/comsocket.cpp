#include "com/comsocket.h"
#include <com/commanager.h>

ComSocket::ComSocket(ComManager *mgr):
    m_mgr(mgr),
    m_work(new boost::asio::io_service::work(m_io)),
    m_t(m_io)

{

}

ComSocket::~ComSocket()
{
    std::cout<<"Socket destruct at: "<<std::this_thread::get_id()<<std::endl;
}

void ComSocket::open()
{
    setupSocket();
    m_thread = std::thread([this] () {
        m_io.run();
    });
}

void ComSocket::setupSocket()
{
    m_t.expires_from_now(boost::posix_time::milliseconds(1));
    m_t.async_wait(boost::bind(&ComSocket::read_msg, this));
}

void ComSocket::send_msg(const mavlink_message_t &msg)
{}


void ComSocket::read_msg()
{
    std::cout<<"Receive at: "<<std::this_thread::get_id()<<std::endl;
    mavlink_message_t msg;
    msg.sysid = 10;
    m_mgr->postData(msg);
    m_t.expires_from_now(boost::posix_time::milliseconds(1));
    m_t.async_wait(boost::bind(&ComSocket::read_msg, this));
}

void ComSocket::endSocket()
{}

void ComSocket::close()
{
    m_io.post(boost::bind(&ComSocket::close_in_worker_thread, this));
    if (m_thread.joinable())
        m_thread.join();
}

void ComSocket::close_in_worker_thread()
{
    std::cout<<"Socket close at: "<<std::this_thread::get_id()<<std::endl;
    endSocket();
    m_work.reset();
    m_io.stop();
}

void ComSocket::send_in_worker_thread(mavlink_message_t msg)
{

}

void ComSocket::do_write()
{

}
