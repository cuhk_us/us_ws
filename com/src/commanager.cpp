#include "com/commanager.h"
#include "com/serialsocket.h"
#include "com/udpsocket.h"
#include "com/utils.h"

ComManager::ComManager(ros::NodeHandle &nh):
    m_nh(nh),
    m_work(new boost::asio::io_service::work(m_io)),
    m_t(m_io),
    m_timer_duration(1),
    m_plugin_loader("com","mavros::MavrosPlugin")
{
    //Subscribe some messages
    m_sub = nh.subscribe("/mavlinkMsg", 10, &ComManager::msgCallback, this);

    std::string device;
    int sys_id;
    int comp_id;
    int baudrate;
    nh.param<std::string>("/com/device", device, "/dev/ttyUSB0");
    nh.param<int>("/com/this_system_id", sys_id, 255);
    nh.param<int>("/com/this_component_id", comp_id, 50);
    nh.param<int>("/com/baudrate", baudrate, 57600);
    SocketList.push_back(new SerialSocket(sys_id,comp_id,device,baudrate,true,this));
    //SocketList.push_back(new UdpSocket(sys_id,comp_id,"127.0.0.1",13999,"192.168.1.10",34569,this));
    for (auto &name : m_plugin_loader.getDeclaredClasses())
        add_plugin(name);
}

ComManager::~ComManager()
{
    std::cout<<"Manager destruct at: "<<std::this_thread::get_id()<<std::endl;
    // first we close all the plugins before deleting all the sockets,
    // because the plugins use sockets internally
    for (auto &plugin : m_loaded_plugins)
    {
        plugin.reset();
    }
    for (auto &socket : SocketList) // access by reference to avoid copying
    {
        socket->close();
        delete socket;
    }
}

void ComManager::rosSpinOnce(const boost::system::error_code &error)
{
    if (error)
    {
        printf("Ros Spin Error: %s\n", error.message().c_str());
        endSpin();
        return;
    }
    if (ros::ok())
    {
        ros::spinOnce();
        m_t.expires_at(m_t.expires_at() + boost::posix_time::milliseconds(m_timer_duration));
        m_t.async_wait(boost::bind(&ComManager::rosSpinOnce, this,
                                   boost::asio::placeholders::error));
    }
    else
    {
        endSpin();
    }
}

void ComManager::exec()
{
    m_t.expires_from_now(boost::posix_time::milliseconds(m_timer_duration));
    m_t.async_wait(boost::bind(&ComManager::rosSpinOnce, this,
                               boost::asio::placeholders::error));

    // Open the socket
    for (auto &socket : SocketList) // access by reference to avoid copying
    {
        socket->open();
    }
    // Start the io service event loop
    m_io.run();
}

void ComManager::endSpin()
{
    m_work.reset();
    m_io.stop();
}

void ComManager::msgCallback(const com::Mavlink::ConstPtr& rmsg)
{
    //    static int i = 0;
    //    std::cout<<i++<<std::endl;
    mavlink_message_t mmsg;
    mavutils::copy_ros_to_mavlink(rmsg, mmsg);
    for (auto &socket : SocketList) // access by reference to avoid copying
    {
        socket->send_msg(mmsg);
    }
}

// in link thread
void ComManager::postData(mavlink_message_t &msg)
{
    m_io.post(boost::bind(&ComManager::processData,this,msg));
}

// in ros thread
void ComManager::processData(mavlink_message_t msg)
{
    if (msg.msgid<0 || msg.msgid > 255)
        return;

    for (auto cb : m_route_table[msg.msgid])
    {
        cb(&msg, msg.sysid, msg.compid);
    }
}

void ComManager::add_plugin(std::string &pl_name)
{
    boost::shared_ptr<mavros::MavrosPlugin> plugin;

    try
    {
        plugin = m_plugin_loader.createInstance(pl_name);
        plugin->initialize(SocketList.front(),m_nh);
        m_loaded_plugins.push_back(plugin);
        std::string repr_name = plugin->getName();

        ROS_INFO_STREAM("Plugin " << repr_name <<" [alias " << pl_name << "] loaded and initialized");

        for (auto &pair : plugin->get_msg_handlers())
        {
            ROS_INFO("Route msgid %d to %s", pair.first, repr_name.c_str());
            m_route_table[pair.first].push_back(pair.second);
        }
    }
    catch (pluginlib::PluginlibException &ex)
    {
        ROS_ERROR_STREAM("Plugin [alias " << pl_name << "] load exception: " << ex.what());
    }
}
