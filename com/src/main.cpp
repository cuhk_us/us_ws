#include "com/commanager.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "com_node");
  ros::NodeHandle nh;
  ComManager c_mgr(nh);
  c_mgr.exec();
  return 0;
}
