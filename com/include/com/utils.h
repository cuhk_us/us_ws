#ifndef UTILS_H
#define UTILS_H
#include <com/Mavlink.h>
#include <mavlink/v1.0/common/mavlink.h>
namespace mavutils {

/**
 * @brief Copy mavros/Mavlink.msg message data to mavlink_message_t
 */
inline bool copy_ros_to_mavlink(const com::Mavlink::ConstPtr &rmsg, mavlink_message_t &mmsg)
{
    if (rmsg->payload64.size() > sizeof(mmsg.payload64) / sizeof(mmsg.payload64[0])) {
        return false;
    }
    mmsg.msgid = rmsg->msgid;
    mmsg.len = rmsg->len;
    std::copy(rmsg->payload64.begin(), rmsg->payload64.end(), mmsg.payload64);
    return true;
}

/**
 * @brief Copy mavlink_message_t to mavros/Mavlink.msg
 */
inline void copy_mavlink_to_ros(const mavlink_message_t *mmsg, com::MavlinkPtr &rmsg)
{
    rmsg->len = mmsg->len;
    rmsg->seq = mmsg->seq;
    rmsg->sysid = mmsg->sysid;
    rmsg->compid = mmsg->compid;
    rmsg->msgid = mmsg->msgid;

    rmsg->payload64.reserve((mmsg->len + 7) / 8);
    for (size_t i = 0; i < (mmsg->len + 7) / 8; i++)
        rmsg->payload64.push_back(mmsg->payload64[i]);
}

} // namespace mavutils
#endif // UTILS_H
