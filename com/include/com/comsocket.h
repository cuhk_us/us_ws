#ifndef COMSOCKET_H
#define COMSOCKET_H

#include <ros/ros.h>
#include <list>
#include <atomic>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <mavlink/v1.0/common/mavlink.h>
#include <thread>
#include <mutex>

#define test_com_socket
class ComManager;
class ComSocket
{
public:
    ComSocket(ComManager* mgr);
    virtual ~ComSocket();
    void open();
    void close();
    virtual void send_msg(const mavlink_message_t &msg);

protected:
    virtual void setupSocket();
    virtual void endSocket();
    virtual void read_msg();
    virtual void send_in_worker_thread(mavlink_message_t msg);
    virtual void do_write();
    void close_in_worker_thread();
    boost::asio::io_service m_io;
    ComManager* m_mgr;
    const uint8_t messageKeys[256] = MAVLINK_MESSAGE_CRCS;

private:
    std::unique_ptr<boost::asio::io_service::work> m_work;
    boost::asio::deadline_timer m_t;
    std::thread m_thread;
};

/**
 * @brief Common exception for communication error
 */
class DeviceError : public std::runtime_error {
public:
    /**
     * @breif Construct error.
     */
    template <typename T>
    DeviceError(const char *module, T msg) :
        std::runtime_error(make_message(module, msg))
    { }

    template <typename T>
    static std::string make_message(const char *module, T msg) {
        std::ostringstream ss;
        ss << "DeviceError:" << module << ":" << msg_to_string(msg);
        return ss.str();
    }

    static std::string msg_to_string(const char *description) {
        return description;
    }

    static std::string msg_to_string(int errnum) {
        return ::strerror(errnum);
    }

    static std::string msg_to_string(boost::system::system_error &err) {
        return err.what();
    }
};

#endif // COMSOCKET_H
