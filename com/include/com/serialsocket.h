#ifndef SERIALSOCKET_H
#define SERIALSOCKET_H

#include <com/comsocket.h>
#include <com/msgbuf.h>

class SerialSocket:public ComSocket, public std::enable_shared_from_this<SerialSocket>
{
public:
    SerialSocket(uint8_t system_id, uint8_t component_id, std::string device, unsigned baudrate, bool hwflow, ComManager* mgr);
    virtual void send_msg(const mavlink_message_t &msg);
protected:
    virtual void setupSocket();
    virtual void endSocket();
    virtual void read_msg();
    virtual void send_in_worker_thread(mavlink_message_t msg);
    virtual void do_write();

private:
    boost::asio::serial_port m_serial_dev;
    std::atomic<bool> m_tx_in_progress;
    std::deque<MsgBuffer> tx_q;
    std::array<uint8_t, MsgBuffer::MAX_SIZE> rx_buf;
    uint8_t m_system_id;
    uint8_t m_component_id;
    std::string m_device;
    unsigned m_baudrate;
    bool m_hwflow;
    bool m_sending;
};

#endif // SERIALSOCKET_H
