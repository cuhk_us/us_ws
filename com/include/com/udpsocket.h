#ifndef UDPSOCKET_H
#define UDPSOCKET_H
#include <com/comsocket.h>
#include <com/msgbuf.h>

class UdpSocket:public ComSocket, public std::enable_shared_from_this<UdpSocket>
{
public:
    UdpSocket(uint8_t system_id, uint8_t component_id, std::string bind_host, unsigned short bind_port, std::string remote_host, unsigned short remote_port, ComManager* mgr);
    virtual void send_msg(const mavlink_message_t &msg);
protected:
    virtual void setupSocket();
    virtual void endSocket();
    virtual void read_msg();
    virtual void send_in_worker_thread(mavlink_message_t msg);
    virtual void do_write();

private:
    boost::asio::ip::udp::socket m_socket;
    boost::asio::ip::udp::endpoint m_bind_ep;
    boost::asio::ip::udp::endpoint m_remote_ep;
    std::deque<MsgBuffer> tx_q;
    std::array<uint8_t, MsgBuffer::MAX_SIZE> rx_buf;
    uint8_t m_system_id;
    uint8_t m_component_id;
    bool m_sending;
};

#endif // UDPSOCKET_H
