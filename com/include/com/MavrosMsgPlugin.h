#ifndef MAVROSMSGPLUGIN_H
#define MAVROSMSGPLUGIN_H
#include <map>
#include <string.h>
#include <vector>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <ros/ros.h>
#include <mavlink/v1.0/common/mavlink.h>

class ComSocket;
namespace mavros
{
#define MESSAGE_HANDLER(_message, _class_method_ptr)	\
{ _message, boost::bind(_class_method_ptr, this, _1, _2, _3) }

class MavrosPlugin
{
public:
    typedef boost::function<void(const mavlink_message_t *msg, uint8_t sysid, uint8_t compid)> message_handler;
    typedef std::map<uint8_t, message_handler> message_map;
    // pluginlib return boost::shared_ptr
    typedef boost::shared_ptr<MavrosPlugin> Ptr;
    typedef boost::shared_ptr<MavrosPlugin const> ConstPtr;

public:
    virtual void initialize(ComSocket *socket, ros::NodeHandle &nh) = 0;
    virtual const message_map get_msg_handlers() = 0;
    virtual std::string getName() = 0;
    virtual ~MavrosPlugin(){}

protected:
    MavrosPlugin(){}

private:
    MavrosPlugin(const MavrosPlugin&) = delete;

protected:
    ComSocket* m_forward_socket;
};
}

#endif // MAVROSMSGPLUGIN_H
