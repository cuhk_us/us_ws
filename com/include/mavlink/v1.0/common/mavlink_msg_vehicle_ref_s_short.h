// MESSAGE VEHICLE_REF_S_SHORT PACKING

#define MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT 155

typedef struct __mavlink_vehicle_ref_s_short_t
{
 int16_t x_ref; /*< x position ref in NED frame*/
 int16_t y_ref; /*< y position ref in NED frame*/
 int16_t z_ref; /*< z position ref in NED frame*/
 int16_t vx_ref; /*< x vel ref in NED frame*/
 int16_t vy_ref; /*< y vel ref in NED frame*/
 int16_t vz_ref; /*< z vel ref in NED frame*/
 int16_t accx_ref; /*< x acc ref in NED frame*/
 int16_t accy_ref; /*< y acc ref in NED frame*/
 int16_t accz_ref; /*< z acc ref in NED frame*/
 int16_t yaw_ref; /*< yaw position ref in NED frame*/
 int16_t yaw_speed_ref; /*< yaw speed ref in NED frame*/
 uint8_t cmd1; /*< cmd to pixhawk*/
} mavlink_vehicle_ref_s_short_t;

#define MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN 23
#define MAVLINK_MSG_ID_155_LEN 23

#define MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC 14
#define MAVLINK_MSG_ID_155_CRC 14



#define MAVLINK_MESSAGE_INFO_VEHICLE_REF_S_SHORT { \
	"VEHICLE_REF_S_SHORT", \
	12, \
	{  { "x_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 0, offsetof(mavlink_vehicle_ref_s_short_t, x_ref) }, \
         { "y_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 2, offsetof(mavlink_vehicle_ref_s_short_t, y_ref) }, \
         { "z_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 4, offsetof(mavlink_vehicle_ref_s_short_t, z_ref) }, \
         { "vx_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 6, offsetof(mavlink_vehicle_ref_s_short_t, vx_ref) }, \
         { "vy_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 8, offsetof(mavlink_vehicle_ref_s_short_t, vy_ref) }, \
         { "vz_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 10, offsetof(mavlink_vehicle_ref_s_short_t, vz_ref) }, \
         { "accx_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 12, offsetof(mavlink_vehicle_ref_s_short_t, accx_ref) }, \
         { "accy_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 14, offsetof(mavlink_vehicle_ref_s_short_t, accy_ref) }, \
         { "accz_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 16, offsetof(mavlink_vehicle_ref_s_short_t, accz_ref) }, \
         { "yaw_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 18, offsetof(mavlink_vehicle_ref_s_short_t, yaw_ref) }, \
         { "yaw_speed_ref", NULL, MAVLINK_TYPE_INT16_T, 0, 20, offsetof(mavlink_vehicle_ref_s_short_t, yaw_speed_ref) }, \
         { "cmd1", NULL, MAVLINK_TYPE_UINT8_T, 0, 22, offsetof(mavlink_vehicle_ref_s_short_t, cmd1) }, \
         } \
}


/**
 * @brief Pack a vehicle_ref_s_short message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param x_ref x position ref in NED frame
 * @param y_ref y position ref in NED frame
 * @param z_ref z position ref in NED frame
 * @param vx_ref x vel ref in NED frame
 * @param vy_ref y vel ref in NED frame
 * @param vz_ref z vel ref in NED frame
 * @param accx_ref x acc ref in NED frame
 * @param accy_ref y acc ref in NED frame
 * @param accz_ref z acc ref in NED frame
 * @param yaw_ref yaw position ref in NED frame
 * @param yaw_speed_ref yaw speed ref in NED frame
 * @param cmd1 cmd to pixhawk
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_vehicle_ref_s_short_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       int16_t x_ref, int16_t y_ref, int16_t z_ref, int16_t vx_ref, int16_t vy_ref, int16_t vz_ref, int16_t accx_ref, int16_t accy_ref, int16_t accz_ref, int16_t yaw_ref, int16_t yaw_speed_ref, uint8_t cmd1)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN];
	_mav_put_int16_t(buf, 0, x_ref);
	_mav_put_int16_t(buf, 2, y_ref);
	_mav_put_int16_t(buf, 4, z_ref);
	_mav_put_int16_t(buf, 6, vx_ref);
	_mav_put_int16_t(buf, 8, vy_ref);
	_mav_put_int16_t(buf, 10, vz_ref);
	_mav_put_int16_t(buf, 12, accx_ref);
	_mav_put_int16_t(buf, 14, accy_ref);
	_mav_put_int16_t(buf, 16, accz_ref);
	_mav_put_int16_t(buf, 18, yaw_ref);
	_mav_put_int16_t(buf, 20, yaw_speed_ref);
	_mav_put_uint8_t(buf, 22, cmd1);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#else
	mavlink_vehicle_ref_s_short_t packet;
	packet.x_ref = x_ref;
	packet.y_ref = y_ref;
	packet.z_ref = z_ref;
	packet.vx_ref = vx_ref;
	packet.vy_ref = vy_ref;
	packet.vz_ref = vz_ref;
	packet.accx_ref = accx_ref;
	packet.accy_ref = accy_ref;
	packet.accz_ref = accz_ref;
	packet.yaw_ref = yaw_ref;
	packet.yaw_speed_ref = yaw_speed_ref;
	packet.cmd1 = cmd1;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
}

/**
 * @brief Pack a vehicle_ref_s_short message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param x_ref x position ref in NED frame
 * @param y_ref y position ref in NED frame
 * @param z_ref z position ref in NED frame
 * @param vx_ref x vel ref in NED frame
 * @param vy_ref y vel ref in NED frame
 * @param vz_ref z vel ref in NED frame
 * @param accx_ref x acc ref in NED frame
 * @param accy_ref y acc ref in NED frame
 * @param accz_ref z acc ref in NED frame
 * @param yaw_ref yaw position ref in NED frame
 * @param yaw_speed_ref yaw speed ref in NED frame
 * @param cmd1 cmd to pixhawk
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_vehicle_ref_s_short_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           int16_t x_ref,int16_t y_ref,int16_t z_ref,int16_t vx_ref,int16_t vy_ref,int16_t vz_ref,int16_t accx_ref,int16_t accy_ref,int16_t accz_ref,int16_t yaw_ref,int16_t yaw_speed_ref,uint8_t cmd1)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN];
	_mav_put_int16_t(buf, 0, x_ref);
	_mav_put_int16_t(buf, 2, y_ref);
	_mav_put_int16_t(buf, 4, z_ref);
	_mav_put_int16_t(buf, 6, vx_ref);
	_mav_put_int16_t(buf, 8, vy_ref);
	_mav_put_int16_t(buf, 10, vz_ref);
	_mav_put_int16_t(buf, 12, accx_ref);
	_mav_put_int16_t(buf, 14, accy_ref);
	_mav_put_int16_t(buf, 16, accz_ref);
	_mav_put_int16_t(buf, 18, yaw_ref);
	_mav_put_int16_t(buf, 20, yaw_speed_ref);
	_mav_put_uint8_t(buf, 22, cmd1);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#else
	mavlink_vehicle_ref_s_short_t packet;
	packet.x_ref = x_ref;
	packet.y_ref = y_ref;
	packet.z_ref = z_ref;
	packet.vx_ref = vx_ref;
	packet.vy_ref = vy_ref;
	packet.vz_ref = vz_ref;
	packet.accx_ref = accx_ref;
	packet.accy_ref = accy_ref;
	packet.accz_ref = accz_ref;
	packet.yaw_ref = yaw_ref;
	packet.yaw_speed_ref = yaw_speed_ref;
	packet.cmd1 = cmd1;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
}

/**
 * @brief Encode a vehicle_ref_s_short struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param vehicle_ref_s_short C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_vehicle_ref_s_short_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_vehicle_ref_s_short_t* vehicle_ref_s_short)
{
	return mavlink_msg_vehicle_ref_s_short_pack(system_id, component_id, msg, vehicle_ref_s_short->x_ref, vehicle_ref_s_short->y_ref, vehicle_ref_s_short->z_ref, vehicle_ref_s_short->vx_ref, vehicle_ref_s_short->vy_ref, vehicle_ref_s_short->vz_ref, vehicle_ref_s_short->accx_ref, vehicle_ref_s_short->accy_ref, vehicle_ref_s_short->accz_ref, vehicle_ref_s_short->yaw_ref, vehicle_ref_s_short->yaw_speed_ref, vehicle_ref_s_short->cmd1);
}

/**
 * @brief Encode a vehicle_ref_s_short struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param vehicle_ref_s_short C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_vehicle_ref_s_short_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_vehicle_ref_s_short_t* vehicle_ref_s_short)
{
	return mavlink_msg_vehicle_ref_s_short_pack_chan(system_id, component_id, chan, msg, vehicle_ref_s_short->x_ref, vehicle_ref_s_short->y_ref, vehicle_ref_s_short->z_ref, vehicle_ref_s_short->vx_ref, vehicle_ref_s_short->vy_ref, vehicle_ref_s_short->vz_ref, vehicle_ref_s_short->accx_ref, vehicle_ref_s_short->accy_ref, vehicle_ref_s_short->accz_ref, vehicle_ref_s_short->yaw_ref, vehicle_ref_s_short->yaw_speed_ref, vehicle_ref_s_short->cmd1);
}

/**
 * @brief Send a vehicle_ref_s_short message
 * @param chan MAVLink channel to send the message
 *
 * @param x_ref x position ref in NED frame
 * @param y_ref y position ref in NED frame
 * @param z_ref z position ref in NED frame
 * @param vx_ref x vel ref in NED frame
 * @param vy_ref y vel ref in NED frame
 * @param vz_ref z vel ref in NED frame
 * @param accx_ref x acc ref in NED frame
 * @param accy_ref y acc ref in NED frame
 * @param accz_ref z acc ref in NED frame
 * @param yaw_ref yaw position ref in NED frame
 * @param yaw_speed_ref yaw speed ref in NED frame
 * @param cmd1 cmd to pixhawk
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_vehicle_ref_s_short_send(mavlink_channel_t chan, int16_t x_ref, int16_t y_ref, int16_t z_ref, int16_t vx_ref, int16_t vy_ref, int16_t vz_ref, int16_t accx_ref, int16_t accy_ref, int16_t accz_ref, int16_t yaw_ref, int16_t yaw_speed_ref, uint8_t cmd1)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN];
	_mav_put_int16_t(buf, 0, x_ref);
	_mav_put_int16_t(buf, 2, y_ref);
	_mav_put_int16_t(buf, 4, z_ref);
	_mav_put_int16_t(buf, 6, vx_ref);
	_mav_put_int16_t(buf, 8, vy_ref);
	_mav_put_int16_t(buf, 10, vz_ref);
	_mav_put_int16_t(buf, 12, accx_ref);
	_mav_put_int16_t(buf, 14, accy_ref);
	_mav_put_int16_t(buf, 16, accz_ref);
	_mav_put_int16_t(buf, 18, yaw_ref);
	_mav_put_int16_t(buf, 20, yaw_speed_ref);
	_mav_put_uint8_t(buf, 22, cmd1);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
#else
	mavlink_vehicle_ref_s_short_t packet;
	packet.x_ref = x_ref;
	packet.y_ref = y_ref;
	packet.z_ref = z_ref;
	packet.vx_ref = vx_ref;
	packet.vy_ref = vy_ref;
	packet.vz_ref = vz_ref;
	packet.accx_ref = accx_ref;
	packet.accy_ref = accy_ref;
	packet.accz_ref = accz_ref;
	packet.yaw_ref = yaw_ref;
	packet.yaw_speed_ref = yaw_speed_ref;
	packet.cmd1 = cmd1;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, (const char *)&packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, (const char *)&packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_vehicle_ref_s_short_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  int16_t x_ref, int16_t y_ref, int16_t z_ref, int16_t vx_ref, int16_t vy_ref, int16_t vz_ref, int16_t accx_ref, int16_t accy_ref, int16_t accz_ref, int16_t yaw_ref, int16_t yaw_speed_ref, uint8_t cmd1)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_int16_t(buf, 0, x_ref);
	_mav_put_int16_t(buf, 2, y_ref);
	_mav_put_int16_t(buf, 4, z_ref);
	_mav_put_int16_t(buf, 6, vx_ref);
	_mav_put_int16_t(buf, 8, vy_ref);
	_mav_put_int16_t(buf, 10, vz_ref);
	_mav_put_int16_t(buf, 12, accx_ref);
	_mav_put_int16_t(buf, 14, accy_ref);
	_mav_put_int16_t(buf, 16, accz_ref);
	_mav_put_int16_t(buf, 18, yaw_ref);
	_mav_put_int16_t(buf, 20, yaw_speed_ref);
	_mav_put_uint8_t(buf, 22, cmd1);

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, buf, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
#else
	mavlink_vehicle_ref_s_short_t *packet = (mavlink_vehicle_ref_s_short_t *)msgbuf;
	packet->x_ref = x_ref;
	packet->y_ref = y_ref;
	packet->z_ref = z_ref;
	packet->vx_ref = vx_ref;
	packet->vy_ref = vy_ref;
	packet->vz_ref = vz_ref;
	packet->accx_ref = accx_ref;
	packet->accy_ref = accy_ref;
	packet->accz_ref = accz_ref;
	packet->yaw_ref = yaw_ref;
	packet->yaw_speed_ref = yaw_speed_ref;
	packet->cmd1 = cmd1;

#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, (const char *)packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT, (const char *)packet, MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE VEHICLE_REF_S_SHORT UNPACKING


/**
 * @brief Get field x_ref from vehicle_ref_s_short message
 *
 * @return x position ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_x_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  0);
}

/**
 * @brief Get field y_ref from vehicle_ref_s_short message
 *
 * @return y position ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_y_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  2);
}

/**
 * @brief Get field z_ref from vehicle_ref_s_short message
 *
 * @return z position ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_z_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  4);
}

/**
 * @brief Get field vx_ref from vehicle_ref_s_short message
 *
 * @return x vel ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_vx_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  6);
}

/**
 * @brief Get field vy_ref from vehicle_ref_s_short message
 *
 * @return y vel ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_vy_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  8);
}

/**
 * @brief Get field vz_ref from vehicle_ref_s_short message
 *
 * @return z vel ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_vz_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  10);
}

/**
 * @brief Get field accx_ref from vehicle_ref_s_short message
 *
 * @return x acc ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_accx_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  12);
}

/**
 * @brief Get field accy_ref from vehicle_ref_s_short message
 *
 * @return y acc ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_accy_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  14);
}

/**
 * @brief Get field accz_ref from vehicle_ref_s_short message
 *
 * @return z acc ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_accz_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  16);
}

/**
 * @brief Get field yaw_ref from vehicle_ref_s_short message
 *
 * @return yaw position ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_yaw_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  18);
}

/**
 * @brief Get field yaw_speed_ref from vehicle_ref_s_short message
 *
 * @return yaw speed ref in NED frame
 */
static inline int16_t mavlink_msg_vehicle_ref_s_short_get_yaw_speed_ref(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int16_t(msg,  20);
}

/**
 * @brief Get field cmd1 from vehicle_ref_s_short message
 *
 * @return cmd to pixhawk
 */
static inline uint8_t mavlink_msg_vehicle_ref_s_short_get_cmd1(const mavlink_message_t* msg)
{
	return _MAV_RETURN_uint8_t(msg,  22);
}

/**
 * @brief Decode a vehicle_ref_s_short message into a struct
 *
 * @param msg The message to decode
 * @param vehicle_ref_s_short C-struct to decode the message contents into
 */
static inline void mavlink_msg_vehicle_ref_s_short_decode(const mavlink_message_t* msg, mavlink_vehicle_ref_s_short_t* vehicle_ref_s_short)
{
#if MAVLINK_NEED_BYTE_SWAP
	vehicle_ref_s_short->x_ref = mavlink_msg_vehicle_ref_s_short_get_x_ref(msg);
	vehicle_ref_s_short->y_ref = mavlink_msg_vehicle_ref_s_short_get_y_ref(msg);
	vehicle_ref_s_short->z_ref = mavlink_msg_vehicle_ref_s_short_get_z_ref(msg);
	vehicle_ref_s_short->vx_ref = mavlink_msg_vehicle_ref_s_short_get_vx_ref(msg);
	vehicle_ref_s_short->vy_ref = mavlink_msg_vehicle_ref_s_short_get_vy_ref(msg);
	vehicle_ref_s_short->vz_ref = mavlink_msg_vehicle_ref_s_short_get_vz_ref(msg);
	vehicle_ref_s_short->accx_ref = mavlink_msg_vehicle_ref_s_short_get_accx_ref(msg);
	vehicle_ref_s_short->accy_ref = mavlink_msg_vehicle_ref_s_short_get_accy_ref(msg);
	vehicle_ref_s_short->accz_ref = mavlink_msg_vehicle_ref_s_short_get_accz_ref(msg);
	vehicle_ref_s_short->yaw_ref = mavlink_msg_vehicle_ref_s_short_get_yaw_ref(msg);
	vehicle_ref_s_short->yaw_speed_ref = mavlink_msg_vehicle_ref_s_short_get_yaw_speed_ref(msg);
	vehicle_ref_s_short->cmd1 = mavlink_msg_vehicle_ref_s_short_get_cmd1(msg);
#else
	memcpy(vehicle_ref_s_short, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_VEHICLE_REF_S_SHORT_LEN);
#endif
}
