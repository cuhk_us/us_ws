//#define USEMAVROS
#include <iostream>
#include <chrono>
#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nndp_cpp/dpdl/RefGenerator.h>
#include <common_msgs/state.h>
#include <std_msgs/Bool.h>
#include <mavlink/v1.0/common/mavlink.h>
#include <tf/transform_broadcaster.h>
#ifdef USEMAVROS
#include "mavros/Mavlink.h"
#include "mavros/utils.h"
namespace messenger = mavros;
#else
#include <com/Mavlink.h>
#include <com/utils.h>
namespace messenger = com;
#endif

#define LOG_TRAJECTORY
RefGenerator *rg=NULL;
Dpdl::vec3d tgt;
ros::Publisher ref_pub;
ros::Publisher ref_state_pub;
ros::Subscriber o_sub;
double heading = 0.0;
bool engaged;
#ifdef LOG_TRAJECTORY
std::ofstream myfile;
#endif

void referenceCallback(const ros::TimerEvent&)
{
    if (!engaged)
        return;

    // Generate the reference state
    RefGenerator::state s = rg->calculateNextCycleReference(tgt);

    //Publish to the drone
    mavlink_message_t msgGe;
    mavlink_msg_vehicle_ref_s_pack(1,2,&msgGe,s.pos[0],-s.pos[1],-s.pos[2],
                                              s.vel[0],-s.vel[1],-s.vel[2],
                                              s.acc[0],-s.acc[1],-s.acc[2],
                                                -heading,0,88,0,0,0,0,0,0);
    messenger::MavlinkPtr rmsg = boost::make_shared<messenger::Mavlink>();
    mavutils::copy_mavlink_to_ros(&msgGe,rmsg);
    ref_pub.publish(rmsg);

    //Publish to the current reference state
    common_msgs::state msgState;
    msgState.pos.x = s.pos.a;
    msgState.pos.y = s.pos.b;
    msgState.pos.z = s.pos.c;

    msgState.vel.x = s.vel.a;
    msgState.vel.y = s.vel.b;
    msgState.vel.z = s.vel.c;

    msgState.acc.x = s.acc.a;
    msgState.acc.y = s.acc.b;
    msgState.acc.z = s.acc.c;

    ref_state_pub.publish(msgState);

#ifdef LOG_TRAJECTORY
    myfile<<s.pos[0]<<" "<<s.pos[1]<<" "<<s.pos[2]<<" "<<
                      s.vel[0]<<" "<<s.vel[1]<<" "<<s.vel[2]<<" "<<
                      s.acc[0]<<" "<<s.acc[1]<<" "<<s.acc[2]<<std::endl;
#endif
}

void tgtCallback(const geometry_msgs::PointStamped::ConstPtr& msg)
{
    tgt.a=msg->point.x;
    tgt.b=msg->point.y;
    tgt.c=msg->point.z;
}

void engageCallback(const std_msgs::Bool::ConstPtr &msg)
{
    engaged = msg->data;
    if (engaged)
        o_sub.shutdown();
}

void get_pose(const geometry_msgs::PoseStamped::ConstPtr &msg)
{
    if(engaged)
        return;

    // update the initial state of reference generator
    RefGenerator::state ini;
    ini.pos.setData(msg->pose.position.x,msg->pose.position.y,msg->pose.position.z);
    rg->setInternalState(ini);

    // update the current yaw value
    double roll,pitch,yaw;
    tf::Quaternion q(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z,msg->pose.orientation.w);
    tf::Matrix3x3 m(q);
    m.getRPY(roll, pitch, yaw);
    heading = yaw;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "rt_ref_gen_cpp");
    ros::NodeHandle nh;
    engaged = false;
#ifdef LOG_TRAJECTORY
    myfile.open("/home/sp/Trajectory.txt");
#endif
    //--- Initialize the ref generator
    std::vector<std::string> names_hor;
    std::vector<std::string> names_ver;
#ifdef VEL_CONTROL
        std::string str = "/home/sp/ctrlpred/vel_1";
#else
        std::string str = "/home/sp/ctrlpred/pos_1";
#endif
    rg = new RefGenerator();
    rg->addController(str);

    //--- Set initial position
    RefGenerator::state ini;
    ini.pos.setData(0,0,0);
    rg->setInternalState(ini);
    tgt = ini.pos;

    ref_pub = nh.advertise<messenger::Mavlink>("mavlink/ros_to_pixhawk", 1);
    ref_state_pub = nh.advertise<common_msgs::state>("rt_ref_gen/current_state",1);
    ros::Subscriber tgt_sub = nh.subscribe("/nndp_cpp/tgt_pos", 1, tgtCallback);
    ros::Subscriber engage_sub = nh.subscribe("/engage_cmd", 1, engageCallback);
    ros::Timer reference_timer = nh.createTimer(ros::Duration(0.05), referenceCallback);
    o_sub = nh.subscribe("/mavros/position/local", 1, get_pose);
    ros::spin();


    delete rg;

#ifdef LOG_TRAJECTORY
    myfile.close();
#endif

    return 0;
}
